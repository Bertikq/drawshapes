package com.triastudios.drawshape.interfaces;

import android.graphics.Canvas;
import android.graphics.Paint;

import com.triastudios.drawshape.models.Point;

public interface IShape extends Cloneable {
    void draw(Canvas canvas);
    void setPaint(Paint paint);
    Paint getPaint();

    boolean inFigure(Point point);

    void move(Point startPoint, Point curPoint);

    IShape clone();
}
