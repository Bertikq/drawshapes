package com.triastudios.drawshape.services;

import android.icu.util.IslamicCalendar;

import com.triastudios.drawshape.enums.ShapeType;
import com.triastudios.drawshape.interfaces.IShape;
import com.triastudios.drawshape.models.Circle;
import com.triastudios.drawshape.models.Point;
import com.triastudios.drawshape.models.Rect;
import com.triastudios.drawshape.models.State;
import com.triastudios.drawshape.models.Triangle;

import java.util.LinkedList;

public class ShapesService {

    private static  ShapesService instance;

    public static ShapesService getInstance() {
        if (instance == null){
            instance = new ShapesService();
        }
        return instance;
    }

    private State state;

    private ShapeType shapeType;

    public ShapesService() {
        state = new State();
    }

    public State getState() {
        return state;
    }

    public LinkedList<IShape> getFigures() {
        return state.getFigures();
    }

    public LinkedList<IShape> getPoints() {
        return state.getPoints();
    }

    public void setPoint(IShape shape){
        state.getPoints().add(shape);
        createFigure();
        SnapShotsService.getInstance().backup();
    }

    public void createFigure(){
        switch (shapeType){
            case Rect:
                if (state.getPoints().size() >= 2){
                    Point p1 = (Point) state.getPoints().pop();
                    Point p2 = (Point) state.getPoints().pop();
                    Rect rect = new Rect(p1, p2);
                    rect.setPaint(p1.getPaint());
                    state.getFigures().add(rect);
                }
            break;
            case Circle:
                if (state.getPoints().size() >= 2){
                    Point p1 = (Point) state.getPoints().pop();
                    Point p2 = (Point) state.getPoints().pop();
                    Circle circle = new Circle(p1, Point.Distance(p1, p2));
                    circle.setPaint(p2.getPaint());
                    state.getFigures().add(circle);
                }
            break;
            case Triangle:
                if (state.getPoints().size() >= 3){
                    Point p1 = (Point) state.getPoints().pop();
                    Point p2 = (Point) state.getPoints().pop();
                    Point p3 = (Point) state.getPoints().pop();
                    Triangle triangle = new Triangle(p1, p2, p3);
                    triangle.setPaint(p3.getPaint());
                    state.getFigures().add(triangle);
                }
            break;
        }
    }

    public void setShapeType(ShapeType shapeType) {
        this.shapeType = shapeType;
    }
}
