package com.triastudios.drawshape.models;

import android.graphics.Paint;

import com.triastudios.drawshape.interfaces.IShape;
import com.triastudios.drawshape.services.SnapShotsService;


public abstract class BaseShape implements IShape {
    protected Paint paint;

    public BaseShape(){
        paint = new Paint();
    }

    @Override
    public void setPaint(Paint paint) {
        this.paint = paint;
    }

    @Override
    public Paint getPaint() {
        return paint;
    }

    @Override
    public IShape clone() {
        return null;
    }
}
