package com.triastudios.drawshape.models;

import com.triastudios.drawshape.interfaces.IShape;

import java.util.LinkedList;

public class State {

    private LinkedList<IShape> points;
    private LinkedList<IShape> figures;

    public State() {
        points = new LinkedList<>();
        figures = new LinkedList<>();
    }

    public LinkedList<IShape> getPoints() {
        return points;
    }

    public LinkedList<IShape> getFigures() {
        return figures;
    }

    public void setFigures(LinkedList<IShape> figures) {
        this.figures = figures;
    }

    public void setPoints(LinkedList<IShape> points) {
        this.points = points;
    }
}
