package com.triastudios.drawshape.models;

import android.graphics.Canvas;
import android.graphics.Paint;

import androidx.annotation.NonNull;

import com.triastudios.drawshape.interfaces.IShape;

public class Rect extends BaseShape {

    private Point luPoint;
    private Point rdPoint;

    public Rect(Point luPoint, Point rdPoint) {
        super();
        this.luPoint = luPoint;
        this.rdPoint = rdPoint;
    }

    @Override
    public void draw(Canvas canvas) {
        android.graphics.Rect rect = new android.graphics.Rect(
                (int)luPoint.getX(), (int)luPoint.getY(),
                (int)rdPoint.getX(), (int)rdPoint.getY());
        canvas.drawRect(rect, paint);
    }

    @Override
    public boolean inFigure(Point point) {
        return luPoint.getX() <= point.getX() &&
                luPoint.getY() <= point.getY() &&
                rdPoint.getX() >= point.getX() &&
                rdPoint.getY() >= point.getY();
    }

    @Override
    public void move(Point startPoint, Point curPoint) {
        float raznX = rdPoint.getX() - luPoint.getX();
        float raznY = rdPoint.getY() - luPoint.getY();
        luPoint = new Point(curPoint.getX() - raznX / 2, curPoint.getY() - raznY / 2);
        rdPoint = new Point(curPoint.getX() + raznX / 2, curPoint.getY() + raznY / 2);
    }

    @Override
    public IShape clone() {
        Rect rect = new Rect((Point) luPoint.clone(), (Point) rdPoint.clone());
        rect.setPaint(paint);
        return rect;
    }
}
