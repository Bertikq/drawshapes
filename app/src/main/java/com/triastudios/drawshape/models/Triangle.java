package com.triastudios.drawshape.models;

import android.graphics.Canvas;
import android.graphics.Path;

import androidx.annotation.NonNull;

import com.triastudios.drawshape.interfaces.IShape;

public class Triangle extends BaseShape {

    private Point p1;
    private Point p2;
    private Point p3;

    public Triangle(Point p1, Point p2, Point p3) {
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
    }

    @Override
    public void draw(Canvas canvas) {

        Path path = new Path();
        path.setFillType(Path.FillType.EVEN_ODD);
        path.moveTo(p1.getX(),p1.getY());
        path.lineTo(p2.getX(),p2.getY());
        path.lineTo(p3.getX(),p3.getY());
        path.close();

        canvas.drawPath(path, paint);
    }

    @Override
    public boolean inFigure(Point point) {
        float a = (p1.getX() - point.getX()) * (p2.getY() - p1.getY()) - (p2.getX() - p1.getX()) * (p1.getY() - point.getY());
        float b = (p2.getX() - point.getX()) * (p3.getY() - p2.getY()) - (p3.getX() - p2.getX()) * (p2.getY() - point.getY());
        float c = (p3.getX() - point.getX()) * (p1.getY() - p3.getY()) - (p1.getX() - p3.getX()) * (p3.getY() - point.getY());

        if ((a >= 0 && b >= 0 && c >= 0) || (a <= 0 && b <= 0 && c <= 0))
        {
            return  true;
        }

        return false;
    }

    @Override
    public void move(Point startPoint, Point curPoint) {

        Point point = new Point((startPoint.getX() - curPoint.getX()) / 10, (startPoint.getY() - curPoint.getY()) / 10);

        p1 = new Point(p1.getX() - point.getX(), p1.getY() - point.getY());
        p2 = new Point(p2.getX() - point.getX(), p2.getY() - point.getY());
        p3 = new Point(p3.getX() - point.getX(), p3.getY() - point.getY());
    }

    @Override
    public IShape clone() {
        Triangle triangle = new Triangle((Point) p1.clone(), (Point)p2.clone(), (Point)p3.clone());
        triangle.setPaint(paint);
        return triangle;
    }
}
