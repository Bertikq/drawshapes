package com.triastudios.drawshape.models;

import android.graphics.Canvas;
import android.graphics.Paint;

import androidx.annotation.NonNull;

import com.triastudios.drawshape.interfaces.IShape;

public class Point extends BaseShape {

    private float x;
    private float y;


    public Point(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public float getY() {
        return y;
    }

    public float getX() {
        return x;
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawPoint(x, y, paint);
    }

    @Override
    public boolean inFigure(Point point) {
        return (x == point.getX()) && (y == point.getY());
    }

    public static float Distance(Point p1, Point p2){
        return (float) Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));
    }

    @Override
    public void move(Point startPoint, Point curPoint) {
        x = curPoint.x;
        y = curPoint.y;
    }

    @Override
    public IShape clone() {
        Point p = new Point(x, y);
        p.setPaint(paint);
        return p;
    }
}
